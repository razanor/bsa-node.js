const express = require('express');
const router = express.Router();

const fighter = require('../controllers/fighter.controller');
const { mustBeInt, bodyLength } = require('../middleware/validation.middleware');
const { validationArray } = require('../helpers/helper');

/* All fighters */
router.get('/', fighter.getFighters);

/* Specific fighter by id */
router.get('/:id', mustBeInt, fighter.getFighter);

/* Create a new fighter */
router.post('/', bodyLength, validationArray, fighter.createFighter);

/* Update a fighter */
router.put('/:id', bodyLength, mustBeInt, validationArray, fighter.updateFighter);

/* Delete a fighter */
router.delete('/:id', mustBeInt, fighter.deleteFighter);

module.exports = router;