const fighterM = require('../models/fighter.model');

// Express-validator package
const { validationResult } = require('express-validator/check');

async function getFighters(req, res) {
    try {
        const fighters = await fighterM.allFighters();
        return res.json(fighters);
    } catch (err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        }
    }
}

async function getFighter(req, res) {
    try {
        const id = req.params.id;
        const fighter = await fighterM.fighterById(id);
        return res.json(fighter);
    } catch (err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        } 
    }
}

async function createFighter(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        const newFighter = await fighterM.insertFighter(req.body);
        return res.status(201).json({
            message: `The fighter #${newFighter._id} has been created`,
            content: newFighter
        });
    } catch(err) {
        res.status(500).json({ message: err.message });
    }
}

async function updateFighter(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        const id = req.params.id;
        const updFighter = await fighterM.updateFighter(id, req.body);
        return res.json({
            message: `The fighter #${id} has been updated`,
            content: updFighter
        });
    } catch(err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        } 
    }
}

async function deleteFighter(req, res) {
    try {
        const id = req.params.id;
        await fighterM.deleteFighter(id);
        return res.json({
            message: `The fighter #${id} has been deleted`
        });
    } catch(err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        }   
    }
}

module.exports = {
    getFighters,
    getFighter,
    createFighter,
    updateFighter,
    deleteFighter
}