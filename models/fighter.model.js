const fs = require('fs');
const helper = require('../helpers/helper.js');
const filename = './data/fighters.json';

/* Read data from file asynchronously */
let fighters = null;
fs.readFile(filename, (err, data) => {
    if (err) throw err;
    fighters = JSON.parse(data);
});

const allFighters = () => {
    return new Promise((resolve, reject) => {
        if (fighters.length == 0) {
            reject({
                message: 'no fighters available',
                status: 202
            })
        }
        resolve(fighters);
    });
}

const fighterById = (id) => {
    return new Promise((resolve, reject) => {
        helper.mustExist(fighters, id)
        .then(fighter => resolve(fighter))
        .catch(err => reject(err))
    });
}

const insertFighter = (fighter) => {
    return new Promise((resolve, reject) => {
        const id = { _id: helper.getNewId(fighters) };
        const newFighter = { ...id, ...fighter };

        fighters.push(newFighter);
        helper.writeJSONFile(filename, fighters);
        resolve(newFighter);
    });
}

const updateFighter = (id, newData) => {
    return new Promise((resolve, reject) => {
        helper.mustExist(fighters, id)
        .then( fighter => {
            const index = fighters.findIndex(f => f._id == fighter._id);
            fighters[index] = { _id: fighter._id, ...newData};
            helper.writeJSONFile(filename, fighters);
            resolve(fighters[index]);
        })
        .catch(err => reject(err));
    });
}

const deleteFighter = (id) => {
    return new Promise((resolve, reject) => {
        helper.mustExist(fighters, id)
        .then(() => {
            fighters = fighters.filter(f => f._id != id);
            helper.writeJSONFile(filename, fighters);
            resolve();
        })
        .catch(err => reject(err));
    })
}

module.exports = {
    allFighters,
    fighterById,
    insertFighter,
    updateFighter,
    deleteFighter
}