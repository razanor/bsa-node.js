#Simple CRUD API express.js
####API routes

+ **GET**: /fighters
+ **GET**: /fighters/:id
+ **POST**: /fighters
+ **PUT**: /fighters/:id
+ **DELETE**: /fighters/:id
                
----
####Packages which are used
+ [**nodemon**](https://www.npmjs.com/package/nodemon)
+ [**morgan**](https://www.npmjs.com/package/morgan)
+ [**cors**](https://www.npmjs.com/package/cors)
+ [**express-validator**](https://www.npmjs.com/package/express-validator)
                
----
####Integretion with ES6 project. Link [here](https://bitbucket.org/razanor/bsa-es6-using-my-api/)
+ Fighters list in main menu
+ Fighter health and attack can be updated in modal window when "save" button clicked
+ Deployed on Heroku. Link [**here**](https://bsa-node-js.herokuapp.com/fighters)

