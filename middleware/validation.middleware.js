const mustBeInt = (req, res, next) => {
    const id = req.params.id;

    if (!Number.isInteger(parseInt(id))) {
        res.status(400).json({ message: 'ID must be an integer'} );
    } else {
        next();
    }
}

const bodyLength = ( req, res, next ) => {
    const len = Object.keys(req.body).length;
    if (len != 5) {
        res.status(400).json({ message: 'Wrong parameters'} );
    } else {
        next();
    }
}

module.exports = {
    mustBeInt,
    bodyLength
}