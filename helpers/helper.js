const fs = require('fs');

// Express-validator package
const { check } = require('express-validator/check');

const validationArray = [
    check('name')
        .isLength({ min: 3})
        .withMessage('must be at least 3 chars long'),
    check('health')
        .isInt( {min: 10} )
        .withMessage('Can\'t be less than 10'),
    check('attack')
        .isInt( {min: 2} )
        .withMessage('Can\'t be less than 2'),
    check('defense')
        .isInt( {min: 1} )
        .withMessage('Can\'t be less than 1'),
    check('source')
        .isURL()
        .withMessage('must be a valid URL')
];

const mustExist = (arr, id) => {
    return new Promise((resolve, reject) => {
        const row = arr.find(r => r._id == id);
        if (!row) {
            reject({
                message: 'No fighter with this ID',
                status: 404
            });
        }
        resolve(row);
    });
}

const getNewId = (arr) => {
    if (arr.length > 0) {
        const newId = +(arr[arr.length - 1]._id) + 1;
        return newId.toString();
    }
    return "1";
}

const writeJSONFile = (filename, content) => {
    fs.writeFile(filename, JSON.stringify(content), 'utf8', (err) => {
        if (err) {
            console.log(err);
        }
    });
}

module.exports = {
    mustExist,
    getNewId,
    writeJSONFile,
    validationArray
}