/* Import packages */
const express = require('express');
const cors = require('cors');
// const morgan = require('morgan'); for development

/* App init */
const app = express();

// app.use(morgan('tiny')); for development
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/fighters', require('./router/fighters.routes'));

/* Errors handling for not existing routes and server errors */
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
});

module.exports = app;

